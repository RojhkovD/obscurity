(function(){


	var scripts = ['https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js', 'jquery.bxslider.min.js'];
	// var scripts = ['https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js', '/projects/Obscurity/jquery.bxslider.min.js'];
	for(var i = 0; i < scripts.length; i++){
		var script = document.createElement('script');
		script.src = scripts[i];
		script.async = false;
		document.body.appendChild(script);
	}

	// listeners only on last script in row
	script.onload = function() {
	  	if (!this.executed) {
	    	this.executed = true;
	    	scriptsReady();
	  	}
	};

	script.onerror = function(){
		console.log('Sorry, this script did not load: ');
		console.dir(this);
	}

	script.onreadystatechange = function() {
	  	var self = this;
	  	if (this.readyState == "complete" || this.readyState == "loaded") {
	    	setTimeout(function() {
	      		self.onload();
	    	}, 0); 
	  	}
	};

	function scriptsReady(){

		var elem = [];
		var e = document.getElementsByClassName('zoom');
		var exit = document.getElementById('exit');
		var images_arr = [];
		var diff = 0;
		for(	var t = 0, i = 0; t<6;t++, i++){
			elem[i] =  document.getElementById('preview' + t);
		}

		//open an image
		function zoom(){

			for(var f = 0;f<6;f++){

				//hover
				elem[f].addEventListener("mouseover", (function(){ 
					var close = f;  
					return function(){
						e[close].style.visibility = "visible";
						e[close].style.opacity = "1";
					}
				})());

				elem[f].addEventListener("mouseout", (function(){
					var close = f;  
					return function(){
						e[close].style.visibility = "hidden";
						e[close].style.opacity = "0";
					}
				})());
			}
		}

		// insert the first 6 image into gallery preview
		function insert(){
			var o = 0;
			var k = 0;
			for(var t = 0; t<images_arr.length&&o<6;t++,o++){
				k = 'preview' + o;
				document.getElementById(k).style.backgroundImage = 'url(' + images_arr[t] + ')';
			}
		}
		//just loads php script, which creates JSON file with images names inside img folder
		function loadPHP(){
		    // var req = new XMLHttpRequest(); 
		    // req.open('GET', 'galery.php', true); 
		    // req.send();
		}
		//creates list of images
		function createLi(){
			for(var t = 0;t<images_arr.length;t++){
				var list = document.createElement('li');
				var image = document.createElement('img');
				image.src = images_arr[t];
				list.appendChild(image);		
				document.getElementById('slider2').appendChild(list);
			}
		}
		//load JSON
		function galeryRequest(){
		    // var req = new XMLHttpRequest();  
		    // req.onreadystatechange = function() { 
		    //     if (req.readyState == XMLHttpRequest.DONE) {
		    //         if(req.status === 200) {
		    //            window.images_arr = JSON.parse(req.responseText);
		    //            insert();
		    //            zoom();
		    //            createLi();
		    //         }
		    //     }
		    // }
		    // req.open('GET', 'images.json', true); 
		    // req.send();


		    images_arr = ['images/gallery/_8YF4wyspTw.jpg', 'images/gallery/2v44pNq8Ufg.jpg', 'images/gallery/4g1-Ry2ImAY.jpg', 'images/gallery/aljuN10KbiU.jpg', 'images/gallery/fSsrVI71oPI.jpg', 'images/gallery/lOxgdbVRKNE.jpg', 'images/gallery/uOALcZFaj9Y.jpg', 'images/gallery/ZHY7P-iPPXM.jpg'];



		    // CHANGE IT IF INSIDE PORTFOLIO
		    //
		    //
		    //
		    //
		    //
		    // images_arr = ['/projects/Obscurity/images/gallery/_8YF4wyspTw.jpg', '/projects/Obscurity/images/gallery/2v44pNq8Ufg.jpg', '/projects/Obscurity/images/gallery/4g1-Ry2ImAY.jpg', '/projects/Obscurity/images/gallery/aljuN10KbiU.jpg', '/projects/Obscurity/images/gallery/fSsrVI71oPI.jpg', '/projects/Obscurity/images/gallery/lOxgdbVRKNE.jpg', '/projects/Obscurity/images/gallery/uOALcZFaj9Y.jpg', '/projects/Obscurity/images/gallery/ZHY7P-iPPXM.jpg'];
		    //
		    //
		    //
		    //
		    //
		    // CHANGE IT IF INSIDE PORTFOLIO


		    insert();
		    zoom();
		    createLi();
		}
		var hide = function(a, e){
			var removeListeners = function(){
				this.listeners.forEach(function(el){
					el.target.removeEventListener(el.e, el.fn);
				});
				this.listeners = [];
			};
			if(e.keyCode === 27 ){
				document.getElementById('galery_image').style.display = "none";
				document.getElementById('wrapper').style.visibility = "hidden";
				a.classList.remove('active');
				removeListeners.apply(hide);
			}else if((e.offsetX > a.offsetWidth - 64 && e.offsetX < a.offsetWidth - 32) && (e.offsetY < 64 && e.offsetY > 32)){
				document.getElementById('galery_image').style.display = "none";
				document.getElementById('wrapper').style.visibility = "hidden";
				a.classList.remove('active');
				removeListeners.apply(hide);
			}
		};
		hide.listeners = [];
		var openSlider = function(){
			document.getElementById('wrapper').style.visibility = "visible";
			document.getElementById('galery_image').style.display = "block";

			Array.prototype.forEach.call(document.querySelectorAll('#slider2 li'), function(a){
				a.classList.add('active');
				a.addEventListener('click', (function(a){
					var listener = hide.bind(null, a),
					eventObject = {
						target: a,
						e: 'click',
						fn: listener
					};
					hide.listeners.push(eventObject);
					return listener;
				}(a)));
				document.addEventListener('keydown', (function(a){
					var listener = hide.bind(null, a),
					eventObject = {
						target: document,
						e: 'keydown',
						fn: listener
					};
					hide.listeners.push(eventObject);
					return listener;
				}(a)));
			});
		}
		//bxslider
		$('#slider1').bxSlider({
			infiniteLoop: false,
		});
		var slider =  $('#slider2').bxSlider({
			slideWidth: 800,
			infiniteLoop: false,
			hideControlOnEnd: true,
			adaptiveHeight: true,
			responsive: true,
			slideMargin: 10,
		});
		$('#preview0').click(function(e){
			openSlider();
			e.preventDefault();
			slider.reloadSlider({
				slideWidth: 800,
				infiniteLoop: false,
				hideControlOnEnd: true,
				adaptiveHeight: true,
				responsive: true,
				slideMargin: 10,
				startSlide: 0,
			});
		});
		$('#preview1').click(function(e){
			openSlider();
			e.preventDefault();
			slider.reloadSlider({
				slideWidth: 800,
				infiniteLoop: false,
				hideControlOnEnd: true,
				adaptiveHeight: true,
				responsive: true,
				slideMargin: 10,
				startSlide: 1,
			});
		}); 
		$('#preview2').click(function(e){
			openSlider();
			e.preventDefault();
			slider.reloadSlider({
				slideWidth: 800,
				infiniteLoop: false,
				hideControlOnEnd: true,
				adaptiveHeight: true,
				responsive: true,
				slideMargin: 10,
				startSlide: 2,
			});
		}); 
		$('#preview3').click(function(e){
			openSlider();
			e.preventDefault();
			slider.reloadSlider({
				slideWidth: 800,
				infiniteLoop: false,
				hideControlOnEnd: true,
				adaptiveHeight: true,
				responsive: true,
				slideMargin: 10,
				startSlide: 3,
			});
		}); 
		$('#preview4').click(function(e){
			openSlider();
			e.preventDefault();
			slider.reloadSlider({
				slideWidth: 800,
				infiniteLoop: false,
				hideControlOnEnd: true,
				adaptiveHeight: true,
				responsive: true,
				slideMargin: 10,
				startSlide: 4,
			});
		});
		$('#preview5').click(function(e){
			openSlider();
			e.preventDefault();
			slider.reloadSlider({
				slideWidth: 800,
				infiniteLoop: false,
				hideControlOnEnd: true,
				adaptiveHeight: true,
				responsive: true,
				slideMargin: 10,
				startSlide: 5,
			});
		});

		document.getElementById('sr').onsubmit = function(e) {
			var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			var myEmail= document.getElementById('email').value;
			var valid = re.test(myEmail);
			if (valid) {
				var output = 'Мы обязательно сообщим вам о новостях нашего Квеста!';
				document.getElementById('message').style.display = "block";
				document.getElementById('message').innerHTML = output;
				document.getElementById('message').style.color = "white";
				document.getElementById('message').style.fontSize = "1em";
				document.getElementById('message').style.fontWeight = '500';
			}
			else {
				var output = 'Адрес электронной почты введен неправильно!';
				document.getElementById('message').style.display = "block";
				document.getElementById('message').innerHTML = output;
				document.getElementById('message').style.color = "red";
				document.getElementById('message').style.fontSize = "1em";
				document.getElementById('message').style.fontWeight = '500';
				document.getElementById('email').value = "";
				e.preventDefault();	
			}
		}  

		// loadPHP();
		galeryRequest();
	}
})();


